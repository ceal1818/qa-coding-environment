# Code Quality Environment

Con este proyecto Vagrant se puede construir un entorno de ideal para comprobar la calidad del código. Esta compuesto de las siguientes herramientas:

* Jenkins
* PMD
* SonarQube

Estas herramientas se distribuiran en dos maquinas:
- node-1: Jenkins y PMD.
- node-2: SonarQube

# Requisitos técnicos #

## Requisitos del sistema ##
- Windows 10.
- RAM: 8Gb mínimo.
- Disco duro: 300Gb mínimo.
- Conexión a Internet: Abierta. 

> **Advertencia**: En caso que la conexión a Internet tenga restricciones es necesario comunicarse con antelación para configurar cada una de las maquinas virtuales, para que puedan adecuarse a esas restricciones.

## Aplicaciones a instalar ##
- **Vagrant 2.1.1**: es una aplicación que permite construir maquinas virtuales y a aprovisionarlas.

    **URL (64 bits)**: https://releases.hashicorp.com/vagrant/2.1.1/vagrant_2.1.1_x86_64.msi

- **VirtualBox 5.2**: Herramienta de virtualización destinada a contener el ambiente de calidad de código.
    
    **URL**: https://download.virtualbox.org/virtualbox/5.2.12/VirtualBox-5.2.12-122591-Win.exe
    
    > **Nota**: Después de instalar virtual es imprecindible instalar el paquete de extensiones. Puede descargarlo desde este enlace: https://download.virtualbox.org/virtualbox/5.2.12/Oracle_VM_VirtualBox_Extension_Pack-5.2.12.vbox-extpack. Para poder instalar este paquete de extensiones es necesario que tenga abierto virtualbox.

# Primeros pasos en el entorno #

Después de descargar este proyecto del repositorio se deben seguir los siguientes pasos para construir ambos entornos (todos estos comandos deben ser ejecutados dentro del directorio del proyecto):

- Ejecutar el comando `vagrant up`. Con este comando se creará las maquinas virtuales y las aprovisionará en su primera ejecución, en las posteriores arrancará las maquinas virtuales.

- Ejecutar el comando `vagrant halt`. Con este comando se detendrán las maquinas virtuales.

- El acceso a las maquinas es a través de SSH con Vagrant, por ello deben ejecutar el siguiente comando: `vagrant ssh [nombre de maquina]`. Los nombres de las maquinas son los siguientes: *node-1* y *node-2*.

- Si las maquinas virtuales quedan inconsistentes, le recomendamos eliminarlas y luego volverlas a crear. Para eliminarlas deben ejecutar el comando `vagrant destroy`, y para volverlas es necesario ejecutar nuevamente el comando `vagrant up`.

> **Importante**: Si la maquina anfitriona es Windows, es necesario modificar el fichero `Vagrantfile`, cambiando el nombre de la red que deseamos sea el enlace al adaptador puente. En la línea 7, justo después de la palabra bridge debemos indicar el nombre del adaptador de red que corresponda en la maquina Windows. Por ejemplo: `node.vm.network "public_network", bridge: "Adaptador de LAN inalámbrica Wi-Fi"`.

# Comprobando servicios

Para comprobar los servicios, es necesario entrar en las maquinas y averiguar las IP de las maquinas. Con la finalidad de facilitar la labor se proporciona el fichero `/home/vagrant/ready` que contiene la IP de la maquina.

En el caso de probar el servicio de Jenkins, basta con abrir un navegador desde la maquina anfitriona y llamar a la URL: `http://[IP node-1]:8080`.

En el caso de probar el servicio de SonarQube, basta con abrir un navegador desde la maquina anfitriona y llamar a la URL: `http://[IP node-2]:9000`.