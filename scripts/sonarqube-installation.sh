#!/bin/bash

if [ -f /home/vagrant/ready ]; then
    cat /home/vagrant/ready
    exit 0
fi

#Install and set up requeriments
apt-get update

apt-get install unzip postgresql postgresql-contrib default-jre default-jdk -y

sudo -u postgres createuser sonar
sudo -u postgres psql -c "ALTER USER sonar WITH ENCRYPTED password '1234'"
sudo -u postgres psql -c "CREATE DATABASE sonar OWNER sonar"

#Install and set up SonarQube
wget https://sonarsource.bintray.com/Distribution/sonarqube/sonarqube-6.4.zip

unzip sonarqube-6.4.zip -d /opt

mv /opt/sonarqube-6.4 /opt/sonarqube

sudo sed -i 's/#sonar.jdbc.username=/sonar.jdbc.username=sonar/g' /opt/sonarqube/conf/sonar.properties
sudo sed -i 's/#sonar.jdbc.password=/sonar.jdbc.password=1234/g' /opt/sonarqube/conf/sonar.properties
sudo sed -i 's/#sonar.jdbc.url=jdbc:postgresql:\/\/localhost\/sonar/sonar.jdbc.url=jdbc:postgresql:\/\/localhost\/sonar/g' /opt/sonarqube/conf/sonar.properties

sudo cat <<EOF >/etc/systemd/system/sonar.service
[Unit]
Description=SonarQube service
After=syslog.target network.target

[Service]
Type=forking

ExecStart=/opt/sonarqube/bin/linux-x86-64/sonar.sh start
ExecStop=/opt/sonarqube/bin/linux-x86-64/sonar.sh stop

User=root
Group=root
Restart=always

[Install]
WantedBy=multi-user.target
EOF

#Run SonarQube as a service
sudo systemctl daemon-reload

sudo systemctl start sonar
sudo systemctl enable sonar

echo `ifconfig enp0s8 2>/dev/null|awk '/inet addr:/ {print $2}'|sed 's/addr://'` > /home/vagrant/ready
cat /home/vagrant/ready
