#!/bin/bash

if [ -f /home/vagrant/ready ]; then
    cat /home/vagrant/ready
else
    #Jenkins Installation.
    apt-get update & apt-get install default-jre default-jdk git unzip -y

    wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -

    sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

    apt-get update

    apt-get install jenkins -y

    wget http://apache.rediris.es/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.zip

    unzip apache-maven-3.5.3-bin.zip -d /opt
    mv /opt/apache-maven-3.5.3 /opt/maven

    #PMD Installation.
    wget https://github.com/pmd/pmd/releases/download/pmd_releases%2F6.4.0/pmd-bin-6.4.0.zip

    unzip pmd-bin-6.4.0.zip -d /opt
    mv /opt/pmd-bin-6.4.0 /opt/pmd

    echo `ifconfig enp0s8 2>/dev/null|awk '/inet addr:/ {print $2}'|sed 's/addr://'` > /home/vagrant/ready
    cat /home/vagrant/ready
fi
